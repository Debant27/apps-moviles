import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ServicesRoutingModule } from "./services-routing.module";
import { ServicesComponent } from "./services.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ServicesRoutingModule
    ],
    declarations: [
        ServicesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ServicesModule { }