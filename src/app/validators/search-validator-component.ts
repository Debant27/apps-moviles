import { Directive, Input } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from "@angular/forms";

@Directive({
    selector: "[minlen]", //El atributo se define en selector, se usa la sintaxis similar a CSS, por eso el nombre del atributo va entre corchetes ‘[]’.
    providers: [{provide: NG_VALIDATORS, useExisting: MinLenDirective, multi: true}]
})

export class MinLenDirective implements Validator {
    @Input() minlen: string;

    constructor() {
    //
    }
    validate(control: AbstractControl): {[key: string]: any} {
        return !control.value || control.value.length >= (this.minlen || 2) ? null : { minlen: true };
    }
}

