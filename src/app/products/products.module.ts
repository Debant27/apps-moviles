import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "../../../node_modules/nativescript-angular/nativescript.module";
import { SettingBusyComponent } from "./products-activity-indicator";

import { ProductsRoutingModule } from "./products-routing.module";
import { ProductsComponent } from "./products.component";

@NgModule({
    imports: [
        NativeScriptModule,
        ProductsRoutingModule
    ],
    declarations: [
        ProductsComponent,
        SettingBusyComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ProductsModule { }
