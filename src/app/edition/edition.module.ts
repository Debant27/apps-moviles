import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "../../../node_modules/nativescript-angular/nativescript.module";
import { MinLenDirective } from "../validators/search-validator-component";
import { EditionRoutingModule } from "./edition-routing.module";
import { EditionComponent } from "./edition.component";

@NgModule({
    imports: [
        NativeScriptModule,
        EditionRoutingModule
    ],
    declarations: [
        EditionComponent,
        MinLenDirective
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class editionModule { }
