import { Component, Input, OnInit, Output } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "Edition",
    template: `
        <FlexboxLayout flexDirection="row">
            <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar un nombre de usuario..." required minlen="4"></TextField>
            <Label *ngIf="texto.hasError('required')" text="*"></Label>
            <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4+"></Label>
        </FlexboxLayout>
        <Button text="Guardar" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
    `
})
export class EditionComponent implements OnInit {
    textFieldValue: string = "";

    @Input() inicial: string;

    ngOnInit(): void {
        // Init your component properties here.
        this.textFieldValue = this.inicial;
    }

    onButtonTap(): void {
        //hacemos la busqueda si la longitud es mayor a 2
        if (this.textFieldValue.length > 4) {
            let LS = require("nativescript-localstorage");
            localStorage.setItem('nombreUsuario',this.textFieldValue);
            
        }
    }
}
