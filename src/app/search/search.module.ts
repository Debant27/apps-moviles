import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
//el siguiente import es para que funcione el two-way bindig
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NoticiasService } from "../domain/noticias.service";
import { SearchFormComponent } from "./search-form-component";
import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { MinLenDirective } from "../validators/search-validator-component"

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SearchComponent,
        SearchFormComponent,
        MinLenDirective
    ],
    //providers: [NoticiasService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }
