import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Notification } from "rxjs";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import  * as app from "tns-core-modules/application";
import * as SocialShare from "nativescript-social-share";
import { NoticiasService } from "../domain/noticias.service";
import {AnimationCurve} from "tns-core-modules/ui/enums";
import * as Toast from "nativescript-toast";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { state } from "@angular/animations";
import { toBase64String } from "@angular/compiler/src/output/source_map";
import { compose } from "nativescript-email";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html",

    //Indica que si este componente (noticias) recibe alguna clase como parametro en su constructor tiene que ser provisto por el proveedor de dependencias de angular (se configura en esta clausula providers).
    //providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<String>;
    @ViewChild ("layout", {static: false}) layout: ElementRef;

    constructor(
        private noticias: NoticiasService,
        private store: Store<AppState>
        ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        /*this.noticias.agregar("Hola!");
        this.noticias.agregar("Hola 2!");
        this.noticias.agregar("Hola 3!");*/

        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null){
                    Toast.makeText("Sugerimos leer: " + f.titulo, "short").show();
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
        console.dir(x);
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    SendEmail(): void {
        const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
        const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
        const appPath = appFolder.path; // esto te da el path a la carpeta src
        const logoPath = appPath + "/app/res/icon.png"; // aquí armas el path del archivo copiado

        compose({
            subject: "Mail de Prueba", // asunto del mail
            body: "Hola <strong>mundo!</strong> :)", // cuerpo que será enviado
            to: ["mail@mail.com"], //lista de destinatarios principales
            cc: [], //lista de destinatarios en copia
            bcc: [], //lista de destinatarios en copia oculta
            attachments: [ //listado de archivos adjuntos
            {
            fileName: "arrow1.png", // este archivo adjunto está en formato base 64 representado por un string
            path: "base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1iYKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmT/oMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC",
            mimeType: "image/png"},
            {
            fileName: "icon.png", // este archivo es el que lees directo del filesystem del mobile
            path: logoPath,
            mimeType: "image/png"
            }]
            }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err));
    }
    buscarAhora(s: string) {
        /*this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
        layout.animate({
            translate: {x: 0, y: 100},
            duration: 500,
            curve: AnimationCurve.easeInOut
        }).then(() => layout.animate({
            translate: {x: 0, y: -100},
            duration: 500,
            curve: AnimationCurve.easeInOut
        }))*/

        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("Error buscarAhora " + e);
            Toast.makeText("Error en la busqueda", "short").show();
        });
    }
}
