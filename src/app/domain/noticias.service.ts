import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
import couchbaseModule = require("nativescript-couchbase");

const Sqlite = require( "nativescript-sqlite" );

@Injectable()
export class NoticiasService {
    database: couchbaseModule.Couchbase;
    api: string = "https://1120e02dc0b3.ngrok.io";
    //private noticias: Array<String> = [];

    constructor(){
        this.database = new couchbaseModule.Couchbase("test-database");
        this.getDB((db) => {
            console.dir(db);
            // el each puede tener una consulta, y el each ejecuta esa consulta contra la base de datos y devuelve un valor (0, 1 o mas filas)
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales)  => console.log("filas totales: ", totales));
        }, () => console.log("error on getDB"));

        this.database.createView("logs", "1", (document, emitter) => emitter.emit(document._id, document)); // le estás pasando: nombre de vista, versión de vista y mapeador de documentos para saber cómo deben verse en la vista.
        const documentos = this.database.executeQuery("logs", {limit : 200});
        console.log("documentos: " + JSON.stringify(documentos));
    }

    getDB(fnOK, fnError){
        //instanciamos al plugin de sqlite, primer parametro es el nombre de la DB(archivo sqlite) esto emite un callback con una bandera o flag de error(err) y una instancia a la base de datos(db)
        return new Sqlite("mi_db_logs", (err, db) => {
            //ver si esta el flag de error levantado si no vamos al else
            if (err) {
                console.error("Error al abrir db!", err);
            }
            else {
                console.log("La db está abierta", db.isOpen() ? "Si" : "No");
                //aqui nos aseguramos que exista la tabla de lo contrario la creamos
                db.execSQL("CREATE TABLE IF NOT EXIST logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                    .then((id) => {
                        console.log("CREATE TABLE OK");
                        fnOK(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    });
            }
        });
    }

    agregar(s: string) {
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
        //this.noticias.push(s);
    }

    favs(){
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {
        this.getDB((db) => {
            db.execSQL("insert into logs (texto) values (?)", [s],
                //callback de ok
                (err, id) => console.log("nuevo id: ", id));
        }, /*Callback de error*/() => console.log("error on getDB"));

        const documentId = this.database.createDocument({ texto: s });
        console.log("nuevo id couchbase: ", documentId);

        return getJSON(this.api + "/get?q=" + s);
    }
}
