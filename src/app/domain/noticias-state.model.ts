import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Observable } from "rxjs";
import { map} from "rxjs/operators";


//***TODA LA LOGICA DE REDUX***/

//Estado
export class Noticia {
    constructor (public titulo: string){ }
}

//tslin:disable-next-line:interface-name
//Aqui tenemos un store que guarda el estado lo definimos a traves de un interface, redux se maneja primero definiendo un estado.
export interface NoticiasState {
    //tslin:disable-next-line:array-type
    items: Noticia[]; //Un array de item de tipo Noticia
    sugerida: Noticia; //Una variable tipo Noticia
}

//Redux necesita que le demos un valor inicial a nuestro store
export function intializeNoticiasState() {
    return {
        items: [],
        sugerida: null
    };
}

//Acciones, redux nos dice que publiquemos acciones que contengan toda la informacion dentro de ellas sin comportamientos, DTO solo tienen propiedades, las acciones siempre tienen que tener un campo type y un stringtype

export enum NoticiasActionTypes {
    INIT_MY_DATA = "[Noticias] Init my Data",
    NUEVA_NOTICIA = "[Noticias] Nueva",
    SUGERIR_NOTICIA = "[Noticias] Sugerir"
}

//tslint:disable-next-line:max-classes-per-file
export class InitMyDataAction implements Action {
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public titulares: Array<string>) {}
}

//tslint:disable-next-line:max-classes-per-file
export class NuevaNoticiaAction implements Action {
    type = NoticiasActionTypes.NUEVA_NOTICIA;
    constructor(public noticia: Noticia) {}
}

//tslint:disable-next-line:max-classes-per-file
export class SugerirAction implements Action {
    type = NoticiasActionTypes.SUGERIR_NOTICIA;
    constructor(public noticia: Noticia) {}
}

//un tipo que es la union de varios tipos enumeracion separado por un pipe, es una bolsa de tipos
export type NoticiasViajesAction = NuevaNoticiaAction | InitMyDataAction;

//Reducers la funcion que tienen dado un estado previo y una accion a realizar agarran el estado anterior y la accion y retorna un nuevo estado
export function reducerNoticias (
    state: NoticiasState,
    action: NoticiasViajesAction
): NoticiasState {
    switch(action.type) {
        case NoticiasActionTypes.INIT_MY_DATA: {
            const titulares: Array<string> = (action as InitMyDataAction).titulares; //con el action castiamos para referenciar la variable titulares
            return {
                //vamos a retornar un nuevo objeto con la clonacion de state y le vamos a agregar el valor de la propiedad items
                ...state,
                //sobreescribirmos items con el nuevo titular
                items: titulares.map((tit) => new Noticia(tit))
            };
        }
        case NoticiasActionTypes.NUEVA_NOTICIA: {
            return {
                ...state,
                //aqui sobre escribimos el array con el valor actual (...state.items) y le agregamos la nueva noticia ((action as NuevaNoticiaAction).noticia)
                items: [...state.items, (action as NuevaNoticiaAction).noticia]
            };
        }
        case NoticiasActionTypes.SUGERIR_NOTICIA: {
            return {
                ...state,
                //solo sobreescribimos la variable sugerida
                sugerida: (action as SugerirAction).noticia
            };
        }
    }
    return state;
}

//Efects
//tslint:disable-next-line:max-classes-per-file
//No modifican el comportamiento sino que dependiendo de la acccion a realizar validan si se tiene que realizar otra accion adicional o invocar algun servicio
@Injectable()
export class NoticiasEffects {
    @Effect()
    //nos suscribimos a las acciones que vayan sucediendo en nuestro store de Redux, en este caso solo interes nuevanoticia
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(NoticiasActionTypes.NUEVA_NOTICIA),
        //mapeamos un sugerir si es una nueva noticia
        map((action: NuevaNoticiaAction) => new SugerirAction(action.noticia))
    );
    constructor(private actions$: Actions) {}
}
