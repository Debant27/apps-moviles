import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { EffectsModule } from "@ngrx/effects";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasService } from "./domain/noticias.service";
import { intializeNoticiasState, NoticiasEffects, NoticiasState, reducerNoticias } from "./domain/noticias-state.model";

//redux init

//Definimos un estado global a este modulo, tenemos un estado global tenemos una propiedad por cada funcionalidad
export interface AppState {
    noticias: NoticiasState;
}

//Definimos el mapeador de reducciones para el estado global, una funcion de reduccion por cada funcionalidad
const reducers: ActionReducerMap<AppState> = { noticias: reducerNoticias };
//Inicializamos el store con un valor por defecto
const reducersInitialState = { noticias: intializeNoticiasState()};
//fin de redux init

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    declarations: [
        AppComponent
    ],
    providers: [NoticiasService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
