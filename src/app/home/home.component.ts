import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(): void {
        camera.requestPermissions().then(
            //permisos concedidos
            function success(){
                //solicitamos una imagen con los siguientes parametros:
                var options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options).then(function (imageAsset) {
                    console.log("Tamaño: " + imageAsset.options.width + "x" + imageAsset.options.height);
                    console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                    console.log("Photo saved in Photos/Gallery for Android or in Camera Roll for iOS");
                    SocialShare.shareImage(imageAsset.nativeImage, "Asunto: Compartido desde el curso")
                }).catch(function (err) {
                    console.log("Error -> " + err.message);
                });
            },
            //permisos denegados
            function failure() {
                console.log("Permiso de camara no aceptado por el usuario");
            }
        );
    }
}
